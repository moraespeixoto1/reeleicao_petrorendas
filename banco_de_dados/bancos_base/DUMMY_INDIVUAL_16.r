#######
# PLEITO DE 2012
ele08 <- read_sav("C:/Users/ilove/OneDrive/Ciências Sociais - UENF/INICIAÇÃO CIENTIFICA/SCRIPTS/reeleição/bancos base/2008 _CAND_Unificado.sav") %>% 
  filter(V9 == 11, V42 == 1) %>% 
  select(V7, V14,V19,V26,V31,V33,V42,V43)
names(ele08)[2] <- "cpf_cand_eleito_ord_2008"

impedidos_12 <- read.csv("vari_reeleicao_2000_2016/reeleitos_08.csv") # OS IMPEDIDOS - reeleitos em 2008

ele12 <- read_sav("C:/Users/ilove/OneDrive/Ciências Sociais - UENF/INICIAÇÃO CIENTIFICA/SCRIPTS/reeleição/bancos base/2012 _CAND_Unificado.sav") %>% 
  filter(V9 == 11, V43 != "2º TURNO") %>% 
  select(V7, V14, V19, V26,V31,V33,V42,V43)
names(ele12)[2] <- "CPF_CANDIDATO_V14"

ele08 <- anti_join(ele08, impedidos_12, by=c("cpf_cand_eleito_ord_2008"="cpf_cand_eleito_ord_2004"))
# Retiram os REELEITOS de 08-12, ou seja, impedidos de reeleito em 2016
# Onde estão os IMPEDIDOS? Na Var. REELEITOS08_12

semtentativa_12 <- anti_join(ele08, ele12, by=c("cpf_cand_eleito_ord_2008"="CPF_CANDIDATO_V14")) 
# OS PREFEITOS *ELEITOS* PELA 1? VEZ EM ele08 /diferente/ TODOS CONCORRENTES DE ele12
# VOLTAM OS DIFERENTES, QUE N?O RE-CONCORRERAM
write_excel_csv(semtentativa_12, "vari_reeleicao_2000_2016/semtentativas_16.csv")

tentativas_12 <- semi_join(ele08, ele12, by=c("cpf_cand_eleito_ord_2008"="CPF_CANDIDATO_V14"), na_matches="never")
# OS *ELEITOS* PELA 1? VEZ EM ele08 /iguais/ TODOS OS CONCORRENTES DE ele12
# VOLTAM OS IGUAIS, QUE TENTARAM
write_excel_csv(tentativas_12, "vari_reeleicao_2000_2016/tentativas_16.csv")

ele12 <- filter(ele12, V42 == 1)
# FILTRO PARA *ELEITOS* 2016

fracasso_12 <- anti_join(ele08, ele12, by=c("cpf_cand_eleito_ord_2008"="CPF_CANDIDATO_V14"))
# OS *ELEITOS* DE ele08 /diferente/ DOS *ELEITOS* DE ele12
# VOLTAM OS QUE FRACASSARAM + OS QUE N?O TENTARAM
fracasso_12 <- semi_join(fracasso_12, tentativas_12, by=c("cpf_cand_eleito_ord_2008"), na_matches="never")
# VOLTAM S? OS QUE TENTATARAM _16 E N?O CONSEGUIRAM, FRACASSARAM
write_excel_csv(fracasso_12, "vari_reeleicao_2000_2016/Fracasso_12.csv")

reeleitos_12 <- semi_join(ele08, ele12, by=c("cpf_cand_eleito_ord_2008"="CPF_CANDIDATO_V14"), na_matches="never")
# VOLTAM OS QUE FORAM ELEITOS EM 12 E TAMB?M EM 16
write_excel_csv(reeleitos_12, "vari_reeleicao_2000_2016/reeleitos_12.csv")

#####################
#  JUNTANDO 2012

reeleitos_12 <- mutate(reeleitos_12, SIT = "1")
fracasso_12 <- mutate(fracasso_12, SIT = "2")
semtentativa_12 <- mutate(semtentativa_12, SIT = "3")
impedidos_12 <- mutate(impedidos_12, SIT = "4")
names(impedidos_12)[4] <- "cpf_cand_eleito_ord_2008"
names(impedidos_12)[5] <- "sigla_partido_cand_eleito_ord_2008"
impedidos_12 <- impedidos_12 %>% 
  rename(V7 = TSE, V19 = sigla_partido_cand_eleito_ord_2008)

pref_cat_12 <- full_join(reeleitos_12, fracasso_12)
pref_cat_12 <- full_join(pref_cat_12, semtentativa_12)
pref_cat_12 <- full_join(pref_cat_12, impedidos_12)

#######
# PLEITO DE 2016

ele12 <- read_sav("vari_reeleicao_2000_2016/pref_eleito_12.sav")
names(ele12)[2] <- "cpf_cand_eleito_ord_2012"

impedidos_16 <- read.csv("vari_reeleicao_2000_2016/reeleitos_12.csv") # OS IMPEDIDOS

ele16 <- read_sav("C:/Users/ilove/OneDrive/Ciências Sociais - UENF/INICIAÇÃO CIENTIFICA/SCRIPTS/reeleição/bancos base/final_2016_LIMPO.sav") %>% 
  filter(CODIGO_CARGO_V9 == 11, DESC_SIT_TOT_TURNO_V43 != "2º TURNO") %>% 
  select(5, 14, 30,32,40,42,51,52)

ele12 <- anti_join(ele12, impedidos_16, by=c("cpf_cand_eleito_ord_2012"="cpf_cand_eleito_ord_2008"))
# Retiram os REELEITOS de 08-12, ou seja, impedidos de reeleito em 2016
# Onde est?o os IMPEDIDOS? Na Var. REELEITOS08_12

semtentativa_16 <- anti_join(ele12, ele16, by=c("cpf_cand_eleito_ord_2012" = "CPF_CANDIDATO_V14")) 
# OS PREFEITOS *ELEITOS* PELA 1? VEZ EM Ele12 /diferente/ TODOS CONCORRENTES DE Ele16
# VOLTAM OS DIFERENTES, QUE N?O RE-CONCORRERAM
write_excel_csv(semtentativa_16, "vari_reeleicao_2000_2016/semtentativas_16.csv")

tentativas_16 <- semi_join(ele12, ele16, by=c("cpf_cand_eleito_ord_2012" = "CPF_CANDIDATO_V14"), na_matches="never")
# OS *ELEITOS* PELA 1? VEZ EM Ele12 /iguais/ TODOS OS CONCORRENTES DE Ele16
# VOLTAM OS IGUAIS, QUE TENTARAM
write_excel_csv(tentativas_16, "vari_reeleicao_2000_2016/tentativas_16.csv")

ele16 <- filter(ele16, COD_SIT_TOT_TURNO_V42 == 1)
# FILTRO PARA *ELEITOS* 2016

fracasso_16 <- anti_join(ele12, ele16, by=c("cpf_cand_eleito_ord_2012" = "CPF_CANDIDATO_V14"))
# OS *ELEITOS* DE Ele12 /diferente/ DOS *ELEITOS* DE Ele16
# VOLTAM OS QUE FRACASSARAM + OS QUE N?O TENTARAM
fracasso_16 <- semi_join(fracasso_16, tentativas_16, by=c("cpf_cand_eleito_ord_2012"), na_matches="never")
# VOLTAM S? OS QUE TENTATARAM _16 E N?O CONSEGUIRAM, FRACASSARAM
write_excel_csv(fracasso_16, "vari_reeleicao_2000_2016/Fracasso_16.csv")

reeleitos_16 <- semi_join(ele12, ele16, by=c("cpf_cand_eleito_ord_2012"="CPF_CANDIDATO_V14"), na_matches="never")
# VOLTAM OS QUE FORAM ELEITOS EM 12 E TAMB?M EM 16
write_excel_csv(reeleitos_16, "vari_reeleicao_2000_2016/reeleitos_16.csv")

##############
# JUNTANDO 2016

reeleitos_16 <- mutate(reeleitos_16, SIT = "1")
fracasso_16 <- mutate(fracasso_16, SIT = "2")
semtentativa_16 <- mutate(semtentativa_16, SIT = "3")
impedidos_16 <- mutate(impedidos_16, SIT = "4")
names(impedidos_16)[4] <- "cpf_cand_eleito_ord_2012"
names(impedidos_16)[5] <- "V19"

pref_cat_16 <- full_join(reeleitos_16, fracasso_16)
pref_cat_16 <- full_join(pref_cat_16, semtentativa_16)
pref_cat_16 <- full_join(pref_cat_16, impedidos_16)

base <- pref_cat_16

pref_cat_16 <- mutate(pref_cat_16, PMDB = ifelse(V19 == "PMDB", 1,0),
                      PSDB = ifelse(V19 == "PSDB", 1,0),
                      PT = ifelse(V19 == "PT", 1,0),
                      PP = ifelse(V19 == "PP", 1,0),
                      PSB = ifelse(V19 == "PSB", 1,0),
                      PSD = ifelse(V19 == "PSD", 1,0),
                      DEM = ifelse(V19 == "DEM", 1,0),
                      PDT = ifelse(V19 == "PDT", 1,0),
                      PR = ifelse(V19 == "PR", 1,0),
                      PTB = ifelse(V19 == "PTB", 1,0),
                      MEDICO = ifelse(V26 == "MÉDICO",1,0),
                      ENS_SUPERIOR = ifelse(V33 == "SUPERIOR COMPLETO",1,0),
                      MULHER = ifelse(V31 == "FEMININO",1,0))
names(pref_cat_16)[1] <- "TSE" 
